﻿namespace LolSkinview_Alpha
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.txtChampionName = new System.Windows.Forms.RichTextBox();
            this.lblOwnedSkin = new MetroFramework.Controls.MetroLabel();
            this.lblOwnedChampions = new MetroFramework.Controls.MetroLabel();
            this.btnGetJson = new MetroFramework.Controls.MetroButton();
            this.exportTo = new MetroFramework.Controls.MetroComboBox();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.my_level = new MetroFramework.Controls.MetroLabel();
            this.myname = new MetroFramework.Controls.MetroLabel();
            this.nowSoloRank = new MetroFramework.Controls.MetroLabel();
            this.skinText = new System.Windows.Forms.RichTextBox();
            this.loadingImg = new System.Windows.Forms.Label();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.nowFlexRank = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.nowTFTRank = new MetroFramework.Controls.MetroLabel();
            this.oldTFTRank = new MetroFramework.Controls.MetroLabel();
            this.oldFlexRank = new MetroFramework.Controls.MetroLabel();
            this.oldSoloRank = new MetroFramework.Controls.MetroLabel();
            this.exportShopV2 = new MetroFramework.Controls.MetroButton();
            this.dataForTool = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton1.DisplayFocus = true;
            this.metroButton1.Highlight = true;
            this.metroButton1.Location = new System.Drawing.Point(234, 22);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(93, 30);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroButton1.TabIndex = 0;
            this.metroButton1.Text = "List Skin Tướng";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // txtChampionName
            // 
            this.txtChampionName.Location = new System.Drawing.Point(232, 88);
            this.txtChampionName.Name = "txtChampionName";
            this.txtChampionName.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.txtChampionName.Size = new System.Drawing.Size(524, 127);
            this.txtChampionName.TabIndex = 3;
            this.txtChampionName.Text = "";
            // 
            // lblOwnedSkin
            // 
            this.lblOwnedSkin.AutoSize = true;
            this.lblOwnedSkin.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnedSkin.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblOwnedSkin.ForeColor = System.Drawing.Color.Red;
            this.lblOwnedSkin.Location = new System.Drawing.Point(24, 111);
            this.lblOwnedSkin.Name = "lblOwnedSkin";
            this.lblOwnedSkin.Size = new System.Drawing.Size(105, 19);
            this.lblOwnedSkin.Style = MetroFramework.MetroColorStyle.Green;
            this.lblOwnedSkin.TabIndex = 4;
            this.lblOwnedSkin.Text = "Skins đã mua: 0";
            this.lblOwnedSkin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOwnedSkin.UseStyleColors = true;
            this.lblOwnedSkin.WrapToLine = true;
            // 
            // lblOwnedChampions
            // 
            this.lblOwnedChampions.AutoSize = true;
            this.lblOwnedChampions.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnedChampions.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblOwnedChampions.ForeColor = System.Drawing.Color.Red;
            this.lblOwnedChampions.Location = new System.Drawing.Point(24, 136);
            this.lblOwnedChampions.Name = "lblOwnedChampions";
            this.lblOwnedChampions.Size = new System.Drawing.Size(113, 19);
            this.lblOwnedChampions.Style = MetroFramework.MetroColorStyle.Green;
            this.lblOwnedChampions.TabIndex = 5;
            this.lblOwnedChampions.Text = "Tướng đã mua: 0";
            this.lblOwnedChampions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOwnedChampions.UseStyleColors = true;
            this.lblOwnedChampions.WrapToLine = true;
            // 
            // btnGetJson
            // 
            this.btnGetJson.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetJson.Location = new System.Drawing.Point(333, 22);
            this.btnGetJson.Name = "btnGetJson";
            this.btnGetJson.Size = new System.Drawing.Size(72, 30);
            this.btnGetJson.Style = MetroFramework.MetroColorStyle.Orange;
            this.btnGetJson.TabIndex = 6;
            this.btnGetJson.Text = "Full Json";
            this.btnGetJson.UseSelectable = true;
            this.btnGetJson.UseStyleColors = true;
            this.btnGetJson.Click += new System.EventHandler(this.btnGetJson_Click);
            // 
            // exportTo
            // 
            this.exportTo.ItemHeight = 23;
            this.exportTo.Items.AddRange(new object[] {
            "Export To Excel",
            "Export To HTML",
            "Export To Json",
            "Show Image"});
            this.exportTo.Location = new System.Drawing.Point(491, 411);
            this.exportTo.Name = "exportTo";
            this.exportTo.PromptText = "Export To";
            this.exportTo.Size = new System.Drawing.Size(121, 29);
            this.exportTo.Sorted = true;
            this.exportTo.TabIndex = 8;
            this.exportTo.Tag = "";
            this.exportTo.UseSelectable = true;
            this.exportTo.SelectedIndexChanged += new System.EventHandler(this.exportTo_SelectedIndexChanged);
            // 
            // metroLink1
            // 
            this.metroLink1.BackColor = System.Drawing.Color.Transparent;
            this.metroLink1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLink1.ForeColor = System.Drawing.Color.Transparent;
            this.metroLink1.Location = new System.Drawing.Point(742, 415);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(49, 25);
            this.metroLink1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLink1.TabIndex = 12;
            this.metroLink1.Text = "Help ?";
            this.metroLink1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLink1.UseSelectable = true;
            this.metroLink1.UseStyleColors = true;
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton2.Location = new System.Drawing.Point(622, 411);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(103, 29);
            this.metroButton2.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroButton2.TabIndex = 13;
            this.metroButton2.Text = "Lưu nội dung";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.UseStyleColors = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.metroLabel1.Location = new System.Drawing.Point(23, 421);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(269, 19);
            this.metroLabel1.TabIndex = 14;
            this.metroLabel1.Text = "Hotline: 0978.296.491. Website: https://hxd.vn";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel1.UseCustomForeColor = true;
            this.metroLabel1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton3.Location = new System.Drawing.Point(560, 22);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(69, 30);
            this.metroButton3.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroButton3.TabIndex = 15;
            this.metroButton3.Text = "Old Shop";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.UseStyleColors = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // my_level
            // 
            this.my_level.AutoSize = true;
            this.my_level.BackColor = System.Drawing.Color.Transparent;
            this.my_level.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.my_level.ForeColor = System.Drawing.Color.Red;
            this.my_level.Location = new System.Drawing.Point(24, 86);
            this.my_level.Name = "my_level";
            this.my_level.Size = new System.Drawing.Size(55, 19);
            this.my_level.Style = MetroFramework.MetroColorStyle.Green;
            this.my_level.TabIndex = 17;
            this.my_level.Text = "Level: 0";
            this.my_level.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.my_level.UseStyleColors = true;
            this.my_level.WrapToLine = true;
            // 
            // myname
            // 
            this.myname.AutoSize = true;
            this.myname.BackColor = System.Drawing.Color.Transparent;
            this.myname.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.myname.ForeColor = System.Drawing.Color.Red;
            this.myname.Location = new System.Drawing.Point(24, 61);
            this.myname.Name = "myname";
            this.myname.Size = new System.Drawing.Size(72, 19);
            this.myname.Style = MetroFramework.MetroColorStyle.Orange;
            this.myname.TabIndex = 16;
            this.myname.Text = "Tên NV: 0";
            this.myname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.myname.UseStyleColors = true;
            this.myname.WrapToLine = true;
            // 
            // nowSoloRank
            // 
            this.nowSoloRank.AutoSize = true;
            this.nowSoloRank.BackColor = System.Drawing.Color.Transparent;
            this.nowSoloRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.nowSoloRank.ForeColor = System.Drawing.Color.Red;
            this.nowSoloRank.Location = new System.Drawing.Point(24, 182);
            this.nowSoloRank.Name = "nowSoloRank";
            this.nowSoloRank.Size = new System.Drawing.Size(102, 19);
            this.nowSoloRank.Style = MetroFramework.MetroColorStyle.Green;
            this.nowSoloRank.TabIndex = 18;
            this.nowSoloRank.Text = "Rank đơn : N/A";
            this.nowSoloRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nowSoloRank.UseStyleColors = true;
            this.nowSoloRank.WrapToLine = true;
            // 
            // skinText
            // 
            this.skinText.Location = new System.Drawing.Point(232, 221);
            this.skinText.Name = "skinText";
            this.skinText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.skinText.Size = new System.Drawing.Size(524, 144);
            this.skinText.TabIndex = 20;
            this.skinText.Text = "";
            // 
            // loadingImg
            // 
            this.loadingImg.BackColor = System.Drawing.Color.Transparent;
            this.loadingImg.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadingImg.Location = new System.Drawing.Point(345, 202);
            this.loadingImg.Name = "loadingImg";
            this.loadingImg.Size = new System.Drawing.Size(308, 47);
            this.loadingImg.TabIndex = 21;
            this.loadingImg.Text = "Đang xử lý dữ liệu";
            this.loadingImg.Visible = false;
            // 
            // metroButton4
            // 
            this.metroButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton4.Location = new System.Drawing.Point(411, 22);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(74, 30);
            this.metroButton4.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroButton4.TabIndex = 22;
            this.metroButton4.Text = "HXD Shop";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.UseStyleColors = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // nowFlexRank
            // 
            this.nowFlexRank.AutoSize = true;
            this.nowFlexRank.BackColor = System.Drawing.Color.Transparent;
            this.nowFlexRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.nowFlexRank.ForeColor = System.Drawing.Color.Red;
            this.nowFlexRank.Location = new System.Drawing.Point(24, 206);
            this.nowFlexRank.Name = "nowFlexRank";
            this.nowFlexRank.Size = new System.Drawing.Size(112, 19);
            this.nowFlexRank.Style = MetroFramework.MetroColorStyle.Green;
            this.nowFlexRank.TabIndex = 24;
            this.nowFlexRank.Text = "Rank Động : N/A";
            this.nowFlexRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nowFlexRank.UseStyleColors = true;
            this.nowFlexRank.WrapToLine = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.ForeColor = System.Drawing.Color.Red;
            this.metroLabel3.Location = new System.Drawing.Point(24, 261);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(115, 19);
            this.metroLabel3.TabIndex = 23;
            this.metroLabel3.Text = "Rank mùa trước";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel3.UseCustomForeColor = true;
            this.metroLabel3.WrapToLine = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.ForeColor = System.Drawing.Color.Red;
            this.metroLabel4.Location = new System.Drawing.Point(24, 161);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(111, 19);
            this.metroLabel4.TabIndex = 25;
            this.metroLabel4.Text = "Rank mùa này :";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroLabel4.UseCustomForeColor = true;
            this.metroLabel4.WrapToLine = true;
            // 
            // nowTFTRank
            // 
            this.nowTFTRank.AutoSize = true;
            this.nowTFTRank.BackColor = System.Drawing.Color.Transparent;
            this.nowTFTRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.nowTFTRank.ForeColor = System.Drawing.Color.Red;
            this.nowTFTRank.Location = new System.Drawing.Point(24, 232);
            this.nowTFTRank.Name = "nowTFTRank";
            this.nowTFTRank.Size = new System.Drawing.Size(65, 19);
            this.nowTFTRank.Style = MetroFramework.MetroColorStyle.Green;
            this.nowTFTRank.TabIndex = 26;
            this.nowTFTRank.Text = "TFT : N/A";
            this.nowTFTRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nowTFTRank.UseStyleColors = true;
            this.nowTFTRank.WrapToLine = true;
            // 
            // oldTFTRank
            // 
            this.oldTFTRank.AutoSize = true;
            this.oldTFTRank.BackColor = System.Drawing.Color.Transparent;
            this.oldTFTRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.oldTFTRank.ForeColor = System.Drawing.Color.Red;
            this.oldTFTRank.Location = new System.Drawing.Point(24, 332);
            this.oldTFTRank.Name = "oldTFTRank";
            this.oldTFTRank.Size = new System.Drawing.Size(65, 19);
            this.oldTFTRank.Style = MetroFramework.MetroColorStyle.Green;
            this.oldTFTRank.TabIndex = 29;
            this.oldTFTRank.Text = "TFT : N/A";
            this.oldTFTRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.oldTFTRank.UseStyleColors = true;
            this.oldTFTRank.WrapToLine = true;
            // 
            // oldFlexRank
            // 
            this.oldFlexRank.AutoSize = true;
            this.oldFlexRank.BackColor = System.Drawing.Color.Transparent;
            this.oldFlexRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.oldFlexRank.ForeColor = System.Drawing.Color.Red;
            this.oldFlexRank.Location = new System.Drawing.Point(24, 307);
            this.oldFlexRank.Name = "oldFlexRank";
            this.oldFlexRank.Size = new System.Drawing.Size(112, 19);
            this.oldFlexRank.Style = MetroFramework.MetroColorStyle.Green;
            this.oldFlexRank.TabIndex = 28;
            this.oldFlexRank.Text = "Rank Động : N/A";
            this.oldFlexRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.oldFlexRank.UseStyleColors = true;
            this.oldFlexRank.WrapToLine = true;
            // 
            // oldSoloRank
            // 
            this.oldSoloRank.AutoSize = true;
            this.oldSoloRank.BackColor = System.Drawing.Color.Transparent;
            this.oldSoloRank.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.oldSoloRank.ForeColor = System.Drawing.Color.Red;
            this.oldSoloRank.Location = new System.Drawing.Point(24, 282);
            this.oldSoloRank.Name = "oldSoloRank";
            this.oldSoloRank.Size = new System.Drawing.Size(102, 19);
            this.oldSoloRank.Style = MetroFramework.MetroColorStyle.Green;
            this.oldSoloRank.TabIndex = 27;
            this.oldSoloRank.Text = "Rank đơn : N/A";
            this.oldSoloRank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.oldSoloRank.UseStyleColors = true;
            this.oldSoloRank.WrapToLine = true;
            // 
            // exportShopV2
            // 
            this.exportShopV2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exportShopV2.Location = new System.Drawing.Point(491, 22);
            this.exportShopV2.Name = "exportShopV2";
            this.exportShopV2.Size = new System.Drawing.Size(63, 30);
            this.exportShopV2.Style = MetroFramework.MetroColorStyle.Orange;
            this.exportShopV2.TabIndex = 30;
            this.exportShopV2.Text = "Shop v2";
            this.exportShopV2.UseSelectable = true;
            this.exportShopV2.UseStyleColors = true;
            this.exportShopV2.Click += new System.EventHandler(this.exportShopV2_Click);
            // 
            // dataForTool
            // 
            this.dataForTool.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataForTool.Location = new System.Drawing.Point(635, 22);
            this.dataForTool.Name = "dataForTool";
            this.dataForTool.Size = new System.Drawing.Size(121, 30);
            this.dataForTool.Style = MetroFramework.MetroColorStyle.Orange;
            this.dataForTool.TabIndex = 31;
            this.dataForTool.Text = "Data for Tool";
            this.dataForTool.UseSelectable = true;
            this.dataForTool.UseStyleColors = true;
            this.dataForTool.Click += new System.EventHandler(this.dataForTool_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 449);
            this.Controls.Add(this.dataForTool);
            this.Controls.Add(this.exportShopV2);
            this.Controls.Add(this.oldTFTRank);
            this.Controls.Add(this.oldFlexRank);
            this.Controls.Add(this.oldSoloRank);
            this.Controls.Add(this.nowTFTRank);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.nowFlexRank);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.skinText);
            this.Controls.Add(this.nowSoloRank);
            this.Controls.Add(this.my_level);
            this.Controls.Add(this.myname);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.exportTo);
            this.Controls.Add(this.btnGetJson);
            this.Controls.Add(this.lblOwnedChampions);
            this.Controls.Add(this.lblOwnedSkin);
            this.Controls.Add(this.txtChampionName);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.loadingImg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.Text = "LOLSkinExport";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.RichTextBox txtChampionName;
        private MetroFramework.Controls.MetroLabel lblOwnedSkin;
        private MetroFramework.Controls.MetroLabel lblOwnedChampions;
        private MetroFramework.Controls.MetroButton btnGetJson;
        private MetroFramework.Controls.MetroComboBox exportTo;
        private MetroFramework.Controls.MetroLink metroLink1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroLabel my_level;
        private MetroFramework.Controls.MetroLabel myname;
        private MetroFramework.Controls.MetroLabel nowSoloRank;
        private System.Windows.Forms.RichTextBox skinText;
        private System.Windows.Forms.Label loadingImg;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroLabel nowFlexRank;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel nowTFTRank;
        private MetroFramework.Controls.MetroLabel oldTFTRank;
        private MetroFramework.Controls.MetroLabel oldFlexRank;
        private MetroFramework.Controls.MetroLabel oldSoloRank;
        private MetroFramework.Controls.MetroButton exportShopV2;
        private MetroFramework.Controls.MetroButton dataForTool;
    }
}

