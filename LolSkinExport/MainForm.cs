﻿using LOL;
using System;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.IO;
using MetroFramework;
using System.Diagnostics;
using System.Security.Principal;
using System.Drawing;
using LolSkinExport.Properties;
using System.Linq;

namespace LolSkinview_Alpha
{
    public partial class MainForm : MetroForm
    {
        private int count = 0;
        private LOLClient _LOLClient = new LOLClient();
        public MainForm()
        {
            InitializeComponent();
            if (_LOLClient == null)
                _LOLClient = new LOLClient();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            exportTo.Enabled = false;
            hideTextBox();
            CheckFree();         
            hideLoading(true);


        }

        private void CheckFree()
        {
          
            if (!_LOLClient.CheckFree())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Opps!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                SetGuiData(true);
            }
        }

        private void SetGuiData(bool full)
        {
            lblOwnedChampions.Text = string.Format("Tổng Tướng: {0}", _LOLClient.TotalChampionsOwned);
            lblOwnedSkin.Text = string.Format("Tổng Skin: {0}", _LOLClient.TotalSkinsOwned);
            myname.Text = string.Format("Tên NV: {0}", _LOLClient.user.displayName);
            my_level.Text = string.Format("Level: {0}", _LOLClient.user.summonerLevel);

            var listRank = _LOLClient.objRank.queues.Select((item, i) => new { Item = item, Index = i });
            //Mùa này
            var RANKED_FLEX_SR = (from queue in listRank
                              where queue.Item.queueType == "RANKED_FLEX_SR"
                                  select queue.Item).First();
            nowFlexRank.Text = string.Format("Rank Động : {0} {1}",
                string.IsNullOrEmpty(RANKED_FLEX_SR.tier) ? "N/A" : RANKED_FLEX_SR.tier,
                RANKED_FLEX_SR.rank);

            var RANKED_SOLO_5x5 = (from queue in listRank
                              where queue.Item.queueType == "RANKED_SOLO_5x5"
                              select queue.Item).First();

            nowSoloRank.Text = string.Format("Rank Đơn : {0} {1}",
                string.IsNullOrEmpty(RANKED_SOLO_5x5.tier) ? "N/A" : RANKED_SOLO_5x5.tier,
               RANKED_SOLO_5x5.rank);

            var RANKED_TFT = (from queue in listRank
                              where queue.Item.queueType == "RANKED_TFT"
                              select queue.Item).First();

            nowTFTRank.Text = string.Format("Rank TFT : {0} {1}",
                string.IsNullOrEmpty(RANKED_TFT.tier) ? "N/A" : RANKED_TFT.tier,
                RANKED_TFT.rank);

           
          
            //Mùa trước, đoạn này lười code quá kkk

            oldSoloRank.Text = string.Format("Rank đơn: {0} {1}",

                string.IsNullOrEmpty(_LOLClient.objRank.queues[0].previousSeasonEndTier) ? "N/A" : _LOLClient.objRank.queues[0].previousSeasonEndTier,              
                _LOLClient.objRank.queues[0].previousSeasonEndRank);

            oldFlexRank.Text = string.Format("Rank Động : {0} {1}",
                string.IsNullOrEmpty(_LOLClient.objRank.queues[1].previousSeasonEndTier) ? "N/A" : _LOLClient.objRank.queues[1].previousSeasonEndTier,
                _LOLClient.objRank.queues[1].previousSeasonEndRank);

            oldTFTRank.Text = string.Format("Rank TFT : {0} {1}",
                string.IsNullOrEmpty(_LOLClient.objRank.queues[2].previousSeasonEndTier) ? "N/A" : _LOLClient.objRank.queues[2].previousSeasonEndTier,
                _LOLClient.objRank.queues[2].previousSeasonEndRank);

            if (full)
            {

                txtChampionName.Text = _LOLClient.strFreeData;
            }
            else
            {
                txtChampionName.Text = _LOLClient.championData;
                skinText.Text = _LOLClient.skinData;
            }
        }

 
        private void btnGetJson_Click(object sender, EventArgs e)
        {
            hideTextBox();
          
            if (!_LOLClient.GetJson())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            exportTo.Enabled = true;
            SetGuiData(true);

            hideLoading(true);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://hxd.vn");
            Process.Start(sInfo);
        }

        private void exportTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (exportTo.SelectedItem == "Export To Excel")
            {
                MetroMessageBox.Show(this, "Chức năng đang được xây dựng !", "Coming soon !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (exportTo.SelectedItem == "Export To HTML")
            {
                MetroMessageBox.Show(this, "Chức năng đang được xây dựng !", "Coming soon !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (exportTo.SelectedItem == "Export To Json")
            {
                MetroMessageBox.Show(this, "Chức năng đang được xây dựng !", "Coming soon !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (exportTo.SelectedItem == "Show Image")
            {
                Bitmap bmp = new Bitmap(this.Width, this.Height);
                this.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                bmp.Save(@"C:\MyPanelImage.bmp");
            }


        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = @"C:\";
            saveFileDialog1.Title = "Save text Files";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = true;
            saveFileDialog1.DefaultExt = "json";
            saveFileDialog1.Filter = "Javascript object file (*.json)|*.json";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveFile(saveFileDialog1.FileName, txtChampionName.Text);
            }
        }

        private void SaveFile(string fileName, string strContent)
        {
            try
            {
                FileInfo fileDeMuc = new FileInfo(fileName);
                if (fileDeMuc.Exists)
                    File.Delete(fileName);
                using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
                {
                    using (StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8))
                    {
                        writer.WriteLine(strContent);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!IsAdministrator())
            {
                DialogResult result = MetroMessageBox.Show(this, "Vui lòng Click chuột phải và chọn Run as Administrator  !", "Running Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Exit();
            }
        }
        public void Exit()
        {
            Environment.Exit(Environment.ExitCode);
        }
        public static bool IsAdministrator()
        {
            return (new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                      .IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("https://hxd.vn");
            Process.Start(sInfo);
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            hideTextBox();
           
            if (!_LOLClient.oldShopCheck())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            exportTo.Enabled = true;
            SetGuiData(false);

            hideLoading(false);

        }

        private void hideTextBox()
        {
            skinText.Visible = false;
            txtChampionName.Visible = false;
            loadingImg.Visible = true;
        }

        private  void hideLoading(bool hideskin)
        {
            loadingImg.Visible = false;
            if(hideskin)
            {
                skinText.Visible = false; 
                txtChampionName.Size = new System.Drawing.Size(524, 283);
            }
            else
            {
                skinText.Visible = true;
                txtChampionName.Size = new System.Drawing.Size(524, 127);
            }
            txtChampionName.Visible = true;
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            hideTextBox();

            if (!_LOLClient.FoxShopCheck())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            exportTo.Enabled = true;
            SetGuiData(false);

            hideLoading(false);
        }

        private void exportShopV2_Click(object sender, EventArgs e)
        {
            hideTextBox();

            if (!_LOLClient.FoxShop2Check())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            exportTo.Enabled = true;
            SetGuiData(false);

            hideLoading(false);
        }


        private void dataForTool_Click(object sender, EventArgs e)
        {
            hideTextBox();

            if (!_LOLClient.GetAllJson())
            {
                MetroMessageBox.Show(this, _LOLClient.Message, "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            exportTo.Enabled = true;
            SetGuiData(true);

            hideLoading(true);
        }
    }
}
