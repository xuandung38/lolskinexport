﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{

    public class Login
    {
        public Logindata logindata { get; set; }
    }

    public class Logindata
    {
        public string email { get; set; }
        public string password { get; set; }
        public string ip { get; set; }
        public string machineid { get; set; }
    }

    public class LoginResponse
    {
        public int status { get; set; }
        public string Message { get; set; }
        public Datum[] Data { get; set; }
    }

    public class Datum
    {
        public string ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string ROLE { get; set; }
        public string EMAIL { get; set; }
        public string SOTIENCON { get; set; }
        public string DATEREG { get; set; }
        public string ACTIVED { get; set; }
        public string TIMELOCK { get; set; }
    }



    public class CheckData
    {
        public Licensedata licensedata { get; set; }
    }

    public class Licensedata
    {
        public string license { get; set; }
        public string nguoidungid { get; set; }
        public string ip { get; set; }
        public string machineid { get; set; }
        public string checkacc { get; set; }
    }

    #region CheckData

    public class CheckResponse
    {
        public int status { get; set; }
        public string Message { get; set; }
        public Check[] Data { get; set; }
    }

    public class Check
    {
        public string ID { get; set; }
        public string NGUOIDUNGID { get; set; }
        public string MACHINEID { get; set; }
        public string NGAYMUA { get; set; }
        public string TYPE { get; set; }
        public string SOLUOTCON { get; set; }
        public string IP { get; set; }
    }

    #endregion

}
