﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    class ExportFull
    {
        public List<Mastery> masteries { get; set; }
        public UserInfo userInfo { get; set; }
        public List<Champion> Champions { get; set; }
        public List<Skin>  Skins { get; set; }
    }

}
