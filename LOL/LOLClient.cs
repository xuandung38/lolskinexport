﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LOL
{
    public class LOLClient
    {
        public LOLClient()
        {
            this.AuthToken = string.Empty;
            this.LocalPort = string.Empty;
        }

        public string AuthToken { get; set; }
        
        public string LocalPort { get; set; }

        Masteries objMasteries { get; set; }

        public UserInfo user { get; set; }
        List<Champion> lstOwnedChampion { get; set; }
        List<Champion> lstNotOwnedChampion { get; set; }
        List<Champion> lstAllChampions { get; set; }

        List<Skin> lstOwnedSkin { get; set; }

        List<Skin> lstNotOwnedSkin { get; set; }
        public int TotalChampionsOwned { get; set; }
        public int TotalSkinsOwned { get; set; }

        public string strFreeData { get; set; }
        public string skinData { get; set; }
        public string championData { get; set; }
        public int TotalChampionsNotOwned { get; set; }
        public int TotalSkinsNotOwned { get; set; }

        public string musername = "";

        public Rank objRank { get; set; }

        public bool Errors = true;

        public string Message = string.Empty;

        public bool GetLeagueCmdData()
        {
            bool result;
            try
            {
                result = (from p in Process.GetProcesses()
                          where p.ProcessName.ToLower().Contains("leagueclient")
                          select p).Any((Process p) => this.GetCommandLine(p));
                List<Process> lstProcess = (from p in Process.GetProcesses()
                                            where p.ProcessName.ToLower().Contains("leagueclient")
                                            select p).ToList();
                if (lstProcess.Count == 0)
                    return false;
                else
                    result = true;
                foreach (Process p in lstProcess)
                {
                    this.GetCommandLine(p);
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                result = false;
            }
            return result;
        }

        private bool GetCommandLine(Process process)
        {
   
            bool result;
            try
            {
                string clientPath = process.MainModule.FileName.Replace(process.MainModule.ModuleName, "");

                string sourceFile = Path.Combine(clientPath, "lockfile");
                string destFile = Path.Combine(clientPath, "lolskinlock");


               File.Copy(sourceFile, destFile, true);

                string lockContent = File.ReadAllText(destFile);
                AuthToken = lockContent.Split(new char[]{':'})[3].Trim();
                LocalPort = lockContent.Split(new char[] { ':' })[2].Trim();

                File.Delete(destFile);

            }
            catch (Exception expr_192)
            {
                Console.WriteLine(expr_192.Message.ToString());
            }
            result = false;
            return result;
        }

        private string GetNumbersOnly(string theString)
        {
            StringBuilder stringBuilder = new StringBuilder(theString.Length);
            int i = 0;
            int length = theString.Length;
            checked
            {
                while (i < length)
                {
                    char c = theString[i];
                    bool flag = char.IsDigit(c);
                    if (flag)
                    {
                        stringBuilder.Append(c);
                    }
                    i++;
                }
                return stringBuilder.ToString();
            }
        }       


        private bool GetWebResponse(out WebResponse response, string URL = "")
        {
            response = null;

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                | SecurityProtocolType.Tls11
                | SecurityProtocolType.Tls12
                | SecurityProtocolType.Ssl3;
                        // allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.KeepAlive = true;
                request.Accept = "application/json; charset=UTF-8;";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36";
                String encoded = System.Convert.ToBase64String(Encoding.GetEncoding("UTF-8").GetBytes("riot:" + this.AuthToken));
                request.Headers.Set(HttpRequestHeader.Authorization, "Basic " + encoded);
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip, deflate, br");
                request.Headers.Set(HttpRequestHeader.AcceptLanguage, "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5");

                response = request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return false;
            }
            catch (Exception)
            {
                if (response != null) response.Close();
                return false;
            }

            return true;
        }

        private string GetResponseString(string URL)
        {
            WebResponse response;
            if (this.GetWebResponse(out response, URL))
            {
                string ret = "";
                Stream res = response.GetResponseStream();
                StreamReader reader = new StreamReader(res);
                ret = reader.ReadToEnd();
                response.Close();
                return ret;
            }
            return string.Empty;
        }

        private void GetUser()
        {
            string URL = string.Format("https://127.0.0.1:{0}/lol-summoner/v1/current-summoner", this.LocalPort);
            string res = this.GetResponseString(URL);
            user = JsonConvert.DeserializeObject<UserInfo>(res);    
        }

        private void GetMastery()
        {
            string URL = string.Format("https://127.0.0.1:{0}/lol-collections/v1/inventories/{1}/champion-mastery/top?limit=3", this.LocalPort, this.user.summonerId);
            string res = this.GetResponseString(URL);
            this.objMasteries = JsonConvert.DeserializeObject<Masteries>(res);
        }

        private void GetChampion()
        {
            string URL = string.Format("https://127.0.0.1:{0}/lol-champions/v1/inventories/{1}/champions", this.LocalPort, this.user.summonerId);
            string res = this.GetResponseString(URL);


            res = Regex.Replace(res, @"""purchaseDate""\s*:\s*[0-9]*\s*,", string.Empty);

            res = Regex.Replace(res, @"""endDate""\s*:\s*[0-9]*\s*,", string.Empty);

            this.lstAllChampions = JsonConvert.DeserializeObject<List<Champion>>(res);

            this.lstOwnedChampion = JsonConvert.DeserializeObject<List<Champion>>(res);

            this.lstNotOwnedChampion = JsonConvert.DeserializeObject<List<Champion>>(res);

            this.lstOwnedChampion = lstOwnedChampion.Where(prop => prop.ownership.owned).ToList();

           
        }

        private void GetRank()
        {
            string URL = string.Format("https://127.0.0.1:{0}/lol-ranked/v1/signed-ranked-stats", this.LocalPort);
            string res = this.GetResponseString(URL);
            objRank = JsonConvert.DeserializeObject<Rank>(res);
        }

        public bool CheckFree()
        {
           
            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                Errors = false;
                return Errors;
            }
            this.GetUser();
            this.GetChampion();
            this.GetRank();
            this.GetMastery();

            List<string> lstOwnedSkin = new List<string>();

            foreach (Champion champ in lstOwnedChampion)
            {
                champ.skins = champ.skins.Where(p => p.ownership.owned && (p.id % 1000) != 0).ToArray();
                lstOwnedSkin.AddRange(champ.skins.Select(p => p.name.ToString()).ToList());
            }

            TotalChampionsOwned = lstOwnedChampion.Count();
            TotalSkinsOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => q.ownership.owned).Count());
            TotalChampionsNotOwned = lstNotOwnedChampion.Count();
            TotalSkinsNotOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => !q.ownership.owned).Count());
            strFreeData = string.Join(", ", lstOwnedChampion.Select(p => p.name));
            string strResult = "Tướng : \n";
            strFreeData = strResult + strFreeData;
            strFreeData += "\n----------------------------------------------------------\n";
            strFreeData += "Trang phục:\n";
            strFreeData += string.Join(", ", lstOwnedSkin);
            return true;
        }


        private string GetWebResponse(string Url)
        {
            WebClient client = new WebClient();
            Byte[] requestedHTML;
            requestedHTML = client.DownloadData(Url);

            UTF8Encoding objUTF8 = new UTF8Encoding();
            string html = objUTF8.GetString(requestedHTML);
            html = StripHTML(html);
            return html;
        }

        private string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?/>", String.Empty);
        }

        public bool oldShopCheck()
        {

            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                Errors = false;
                return Errors;
            }
            this.GetUser();
            this.GetChampion();
            this.GetRank();
            this.GetMastery();

            List<string> lstOwnedSkin = new List<string>();

            foreach (Champion champ in lstOwnedChampion)
            {
                champ.skins = champ.skins.Where(p => p.ownership.owned && (p.id % 1000) != 0).ToArray();
                lstOwnedSkin.AddRange(champ.skins.Select(p => "championsskin_" + p.id.ToString() + "-" + p.name.ToString()).ToList());
            }

            TotalChampionsOwned = lstOwnedChampion.Count();
            TotalSkinsOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => q.ownership.owned).Count());
            TotalChampionsNotOwned = lstNotOwnedChampion.Count();
            TotalSkinsNotOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => !q.ownership.owned).Count());
            championData = string.Join("|", lstOwnedChampion.Select(p => p.id + "-" + p.name));          
            skinData = string.Join("|", lstOwnedSkin);
            return true;
        }
        public bool FoxShopCheck()
        {

            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                Errors = false;
                return Errors;
            }
            this.GetUser();
            this.GetChampion();
            this.GetRank();
            this.GetMastery();

            List<string> lstOwnedSkin = new List<string>();

            foreach (Champion champ in lstOwnedChampion)
            {
                champ.skins = champ.skins.Where(p => p.ownership.owned && (p.id % 1000) != 0).ToArray();
                lstOwnedSkin.AddRange(champ.skins.Select(p => p.id.ToString() + "-" + p.name.ToString()).ToList());
            }

            TotalChampionsOwned = lstOwnedChampion.Count();
            TotalSkinsOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => q.ownership.owned).Count());
            TotalChampionsNotOwned = lstNotOwnedChampion.Count();
            TotalSkinsNotOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => !q.ownership.owned).Count());
            championData = string.Join("|", lstOwnedChampion.Select(p => p.id + "-" + p.name));
            skinData = string.Join("|", lstOwnedSkin);
            return true;
        }

        public bool FoxShop2Check()
        {

            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                Errors = false;
                return Errors;
            }
            this.GetUser();
            this.GetChampion();
            this.GetRank();
            this.GetMastery();

            List<string> lstOwnedSkin = new List<string>();

            foreach (Champion champ in lstOwnedChampion)
            {
                champ.skins = champ.skins.Where(p => p.ownership.owned && (p.id % 1000) != 0).ToArray();
                lstOwnedSkin.AddRange(champ.skins.Select(p => p.name.ToString() + "_" + p.id.ToString()).ToList());
            }

            TotalChampionsOwned = lstOwnedChampion.Count();
            TotalSkinsOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => q.ownership.owned).Count());
            TotalChampionsNotOwned = lstNotOwnedChampion.Count();
            TotalSkinsNotOwned = lstOwnedChampion.Sum(p => p.skins.Where(q => !q.ownership.owned).Count());
            championData = string.Join("|", lstOwnedChampion.Select(p =>p.name + "_" + p.id));
            skinData = string.Join("|", lstOwnedSkin);
            return true;
        }

        public bool GetJson()
        {

            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                return false;
            }

            if (CheckFree())
            {
                Export objExport = new Export()
                {
                    userInfo = this.user,
                    masteries = this.objMasteries.masteries,
                    rankedData = this.objRank
                };
                objExport.champs = (from p in this.lstOwnedChampion
                                    where p.ownership.owned
                                    select new Champ()
                                    {
                                        id = p.id.ToString(),
                                        alias = p.alias,
                                        name = p.name,
                                        avatar = string.Format("https://cdnshop.hxd.vn/champs/{0}.jpg", p.id.ToString())
                                    }).ToList();
                objExport.skins = new List<SkinExport>();
                foreach (Champion champ in this.lstOwnedChampion)
                {
                    List<SkinExport> lstOwnedSkin = (from p in champ.skins
                                                     where p.ownership.owned
                                                     select new SkinExport()
                                                     {
                                                         id = p.id.ToString(),
                                                         idchamp = p.championId.ToString(),
                                                         name = p.name,
                                                         image = string.Format("https://cdnshop.hxd.vn/skins/{0}.jpg", p.id.ToString())
                                                     }).ToList();
                    objExport.skins.AddRange(lstOwnedSkin);
                }
                strFreeData = JsonConvert.SerializeObject(new List<Export>() { objExport }, Formatting.Indented);
                return true;
            }
            return false;
        }

        public bool GetAllJson()
        {

            if (!this.GetLeagueCmdData())
            {
                Message = "Bạn phải mở LMHT trước !";
                return false;
            }

            if (CheckFree())
            {
                Export objExport = new Export()
                {
                    authToken = this.AuthToken,
                    userInfo = this.user,
                    masteries = this.objMasteries.masteries,
                    rankedData = this.objRank,
                    champs = null,
                    skins = null
                };

                objExport.champs = (from p in this.lstAllChampions
                                    select new Champ()
                                    {
                                        id = p.id.ToString(),
                                        alias = p.alias,
                                        name = p.name,
                                        avatar = string.Format("https://127.0.0.1:{0}{1}", this.LocalPort, p.squarePortraitPath)
                                    }).ToList();
                objExport.skins = new List<SkinExport>();
                foreach (Champion champ in this.lstAllChampions)
                {
                    List<SkinExport> lstOwnedSkin = (from p in champ.skins
                                                     select new SkinExport()
                                                     {
                                                         id = p.id.ToString(),
                                                         idchamp = p.championId.ToString(),
                                                         name = p.name,
                                                         image = string.Format("https://127.0.0.1:{0}{1}", this.LocalPort, p.tilePath)
                                                     }).ToList();
                    objExport.skins.AddRange(lstOwnedSkin);
                }
                strFreeData = JsonConvert.SerializeObject(new List<Export>() { objExport }, Formatting.Indented);
                return true;
            }
            return false;
        }
    }

}
